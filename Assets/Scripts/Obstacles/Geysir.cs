using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Geysir : MonoBehaviour
{
    public float EmissionTiming = 4;
    public float Variance = 2F;

    public float spitHeight = 1F;

    [HideInInspector] public bool Active = true;

    public UnityEngine.Mesh PreviewMesh;
    [SerializeField] private float UpwardForce = 10F;

    private ParticleSystem _particles;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpitTimer());
        _particles = GetComponentInChildren<ParticleSystem>();
    }

    IEnumerator SpitTimer()
    {
        while (Active)
        {
            float Timer = Random.Range(EmissionTiming - Variance, EmissionTiming + Variance);
            yield return new WaitForSeconds(Timer);

            LaunchUpwards();
        }
    }

    private void LaunchUpwards()
    {
        // Get nearby objects
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1F);
        _particles.Emit(10);
        foreach (Collider collider1 in colliders)
        {
            Rigidbody rb = collider1.GetComponent<Rigidbody>();
            if (rb)
            {
                rb.AddForceAtPosition(UpwardForce * Vector3.up, transform.position, ForceMode.Impulse);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Color c = Color.HSVToRGB(32F/360.0F, 0.98F, 0.91F);
        c.a = 0.3F;
        Gizmos.color = c;
        Gizmos.DrawMesh(PreviewMesh, transform.position + ( Vector3.up * spitHeight), Quaternion.identity, new Vector3(1, spitHeight, 1));
    }
}