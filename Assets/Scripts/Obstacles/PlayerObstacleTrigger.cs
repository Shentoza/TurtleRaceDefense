#region Import

using Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

#endregion

public class PlayerObstacleTrigger : MonoBehaviour
{
    public UnityEvent<GameObject> OnPlayerEntered;


    private void OnTriggerEnter(Collider other)
    {
        var player = other.CompareTag("Player");
        if (player)
        {
            OnPlayerEntered.Invoke(other.attachedRigidbody.gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Color c = Color.yellow;
        c.a = 0.3F;
        Gizmos.color = c;
        SphereCollider sCollider = GetComponentInChildren<SphereCollider>();
        if (sCollider)
        {
            Gizmos.DrawSphere(transform.position, sCollider.radius * transform.lossyScale.MaxElement());
        }

        BoxCollider bCollider = GetComponentInChildren<BoxCollider>();
        if (bCollider)
        {
            Vector3 s = bCollider.size;
            s.Scale(transform.lossyScale);
            Gizmos.DrawCube(transform.position + bCollider.center, s);
        }
    }
}
