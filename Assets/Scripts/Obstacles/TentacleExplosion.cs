using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleExplosion : MonoBehaviour
{
    [SerializeField] private float impactRadius = 1.5F;
    [SerializeField] private float impactPushbackForce = 10F;

    [SerializeField] private Transform slapLocation;
    [SerializeField] private ParticleSystem particles;
    
    public void CauseHitImpact()
    {
        Vector3 impactLocation = slapLocation.position;


        Collider[] hits = Physics.OverlapSphere(impactLocation, impactRadius);
        if (particles.isPlaying)
        {
            particles.Stop();
        }
        particles.Play();

        foreach (Collider hit in hits)
        {
            var rb = hit.GetComponent<Rigidbody>();
            if (rb)
            {
                rb.AddExplosionForce(impactPushbackForce, impactLocation, impactRadius, 1F, ForceMode.Impulse);
            }
        }
    }


    private void OnDrawGizmos()
    {
        Color c = Color.HSVToRGB(32F / 360.0F, 0.98F, 0.91F);
        c.a = 0.3F;
        Gizmos.color = c;
        Gizmos.DrawSphere(slapLocation.position, impactRadius);
    }
}