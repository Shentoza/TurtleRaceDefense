﻿// Copyright(C) battyRabbit UG (limited liability). All Rights Reserved.

using System;
using Extensions;
using ModifierStack;
using UnityEngine;

namespace Obstacles
{
    public class Seeigel : MonoBehaviour
    {
        
        
        private void OnTriggerEnter(Collider other)
        {
            var car = other.GetComponent<CarController>();
            if (car)
            {
                car.SpeedModifierStack.PushModifier(new ModifierStackValue(0.5F, this.gameObject));
            }
        }

        private void OnTriggerExit(Collider other)
        {
            var car = other.GetComponent<CarController>();
            if (car)
            {
                car.SpeedModifierStack.RemoveAllModifiersFromSource(this.gameObject);
            }
        }

        private void OnDrawGizmos()
        {
            Color c = Color.HSVToRGB(32F/360.0F, 0.98F, 0.91F);
            c.a = 0.3F;
            Gizmos.color = c;
            Gizmos.DrawSphere(transform.position, GetComponentInChildren<SphereCollider>().radius * transform.lossyScale.MaxElement());            
        }

        
    }
}