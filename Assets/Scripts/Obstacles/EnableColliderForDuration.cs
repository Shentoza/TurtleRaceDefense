using System;
using System.Collections;
using System.Collections.Generic;
using Extensions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

public class EnableColliderForDuration : MonoBehaviour
{
    [SerializeField] private Collider _collider;
    
    public void EnableCollider()
    {
        _collider.enabled = true;
    }

    public void DisableCollider()
    {
        _collider.enabled = false;
    }

    private void OnDrawGizmos()
    {
        Color c = Color.HSVToRGB(32F / 360.0F, 0.98F, 0.91F);
        c.a = 0.3F;
        Gizmos.color = c;

        Vector3 textLocation = _collider.transform.position;
        SphereCollider sCollider = _collider as SphereCollider;
        if (sCollider)
        {
            Gizmos.DrawSphere(transform.position, sCollider.radius * transform.lossyScale.MaxElement());
        }

        BoxCollider bCollider = _collider as BoxCollider;
        if (bCollider)
        {
            Vector3 s = bCollider.size;
            s.Scale(transform.lossyScale);
            Gizmos.DrawCube(transform.position + bCollider.center, s);
            textLocation += bCollider.center;
        }

        GUIStyle guiStyle = new GUIStyle();
        guiStyle.fontSize = 18;
        guiStyle.alignment = TextAnchor.MiddleCenter;
        Handles.color = c;
        Handles.Label(textLocation, "Toggleable Collider", guiStyle);

    }
}
