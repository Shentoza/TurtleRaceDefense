using System;
using System.Collections;
using System.Collections.Generic;
using Extensions;
using UnityEngine;

public class ExplodeOnImpact : MonoBehaviour
{
    public float ExplosionRadius = 2F;
    public float ExplosionImpulse = 10F;

    private void OnCollisionEnter(Collision other)
    {
        Vector3 impactLocation = transform.position;


        Collider[] hits = Physics.OverlapSphere(impactLocation, ExplosionRadius);

        foreach (Collider hit in hits)
        {
            var rb = hit.GetComponent<Rigidbody>();
            if (rb)
            {
                rb.AddExplosionForce(ExplosionImpulse, transform.position, ExplosionRadius, 0F, ForceMode.Impulse);
            }
        }

        GameObject.Destroy(this.gameObject);
    }


    private void OnDrawGizmos()
    {
        Color c = Color.HSVToRGB(32F / 360.0F, 0.98F, 0.91F);
        c.a = 0.3F;
        Gizmos.color = c;
        Gizmos.DrawSphere(transform.position, GetComponentInChildren<SphereCollider>().radius * transform.lossyScale.MaxElement());            

    }
}