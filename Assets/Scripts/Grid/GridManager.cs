#region Import

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Grid.UI;
using Grid.Tiles;
using System;
using System.IO;
using PlayerBuildMode;

#endregion

namespace Grid
{
    public class GridManager
    {
        private static List<GridManager> _managers = new List<GridManager>();

        /// <summary>
        /// An empty cell in the grid
        /// </summary>
        private GameObject _emptyCell;

        /// <summary>
        /// A particle effect to visualize the current cell
        /// </summary>
        private SelectionVisualizer _cellVisualizer;

        /// <summary>
        /// The current active tile preview
        /// </summary>
        private GridTile previewInstance;

        /// <summary>
        /// The blueprint for the preview object
        /// </summary>
        private GridTile previewBluePrint;

        /// <summary>
        /// Spawned object cache
        /// </summary>
        private Dictionary<Vector3, GridTile> _placedTiles;

        /// <summary>
        /// The current active eventSystem
        /// </summary>
        private EventSystem _eventSystem;

        /// <summary>
        /// Vector describing the half size of a cell
        /// </summary>
        private Vector3 _halfCellSizeVector;

        /// <summary>
        /// Thrown when a tile is built
        /// </summary>
        public event Action<GridTile> EventTileBuilt;

        /// <summary>
        /// Thrown when a tile is deleted
        /// </summary>
        public event Action<GridTile> EventTileDeleted;
        
        private PlayerBuildModeManager PlayerBuildModeManager { get; set; }

        private int GridSizeInCells => PlayerBuildModeManager.AreaDefinition.GridSizeInCells;

        private float GridCellSize => PlayerBuildModeManager.AreaDefinition.CellSize;

        private GameObject _map;

        private bool _deleteMode = false;
        private bool DeleteMode {
            get => _deleteMode;
            set {
                if (value == _deleteMode) return;
                _deleteMode = value;
                if (_deleteMode)
                {
                    if (previewInstance != null)
                        previewInstance.gameObject.SetActive(false);
                    _cellVisualizer.gameObject.SetActive(true);
                    _cellVisualizer.SetColorState(false);
                }
                else
                {
                    _cellVisualizer.SetColorState(true);
                    if (previewInstance != null)
                    {
                        previewInstance.gameObject.SetActive(true);
                        _cellVisualizer.gameObject.SetActive(false);
                    }
                }

            }
        }

        public void Initialize(PlayerBuildModeManager pBM, GameObject emptyCell, SelectionVisualizer selector)
        {
            if(!_managers.Contains(this))
                _managers.Add(this);
            PlayerBuildModeManager = pBM;
            _map = new GameObject("Map");
            _map.transform.SetParent(pBM.transform);
            _emptyCell = emptyCell;
            _cellVisualizer = selector;
            _placedTiles = new Dictionary<Vector3, GridTile>();
            _eventSystem = EventSystem.current;
            
            var halfCellSize = GridCellSize * 0.5f;
            _halfCellSizeVector = new Vector3(halfCellSize, 0.0f, halfCellSize);

            selector.transform.localScale = pBM.AreaDefinition.CellSize * selector.transform.localScale;
            SpawnGrid();
        }

        /// <summary>
        /// Todo do we need this?
        /// </summary>
        public void CleanUp()
        {
            if (_managers.Contains(this))
                _managers.Remove(this);
        }

        /// <summary>
        /// Genereate a preview for the selected UI button
        /// Deactivate  grid cell visualizer
        /// </summary>
        public void OnEventButtonSelected(GridTileSelectionButton b)
        {
            OnEventButtonDeselected();
            previewBluePrint = b.tileLibraryTileEntry.tile;
            previewInstance = UnityEngine.Object.Instantiate(previewBluePrint, _map.transform);
            previewInstance.CheckObstaclesOnPlaced();
            _cellVisualizer.gameObject.SetActive(false);
        }

        /// <summary>
        /// The current selected UI button was deselected
        /// Clear the preview and reactivate grid cell visualizer
        /// </summary>
        public void OnEventButtonDeselected()
        {
            if (previewInstance == null) return;
            _cellVisualizer.gameObject.SetActive(true);
            UnityEngine.Object.Destroy(previewInstance.gameObject);
            previewInstance = previewBluePrint = null;
        }

        /// <summary>
        /// Spawn grid
        /// Todo generate texture to visualize grid? 
        /// Use one big box collider to create hit plane for the grid
        /// </summary>
        private void SpawnGrid()
        {
            var holder = new GameObject("EmptyGrid");
            holder.transform.localScale = GridCellSize * Vector3.one;
            holder.transform.SetParent(PlayerBuildModeManager.transform);
            float halfLengtAndWidth = GridSizeInCells * 0.5f;
            Vector3 startPoint = new Vector3(-halfLengtAndWidth * GridCellSize, 0.0f, -halfLengtAndWidth * GridCellSize) + _halfCellSizeVector;

            for (int x = 0; x < GridSizeInCells; x++)
            {
                for (int z = 0; z < GridSizeInCells; z++)
                {
                    var pos = startPoint + new Vector3(x * GridCellSize, 0.0f, z * GridCellSize);
                    UnityEngine.Object.Instantiate(_emptyCell, pos, Quaternion.identity, holder.transform);
                }
            }
        }
        
        public void DoUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                GenerateSaveFile();
            if (!HitSomething(out var gridPos)) return;
            if (_eventSystem.IsPointerOverGameObject()) return;
            DeleteMode = Input.GetKey(KeyCode.D);
            if (CheckObjectRotate()) return;
            if (CheckPlacement(gridPos)) return;
            CheckDelete(gridPos);
        }

        /// <summary>
        /// Did we hit something on the grid?
        /// </summary>
        private bool HitSomething(out Vector3 gridPos)
        {
            var ray = PlayerBuildModeManager.playerCamera.ScreenPointToRay(Input.mousePosition);
            if (!PlayerBuildModeManager.AreaDefinition.HasRayValidPosition(ray, out var hit))
            {
                gridPos = Vector3.zero;
                return false;
            }
            
            gridPos = new Vector3Int(
                    Mathf.FloorToInt(Mathf.FloorToInt(hit.x/ GridCellSize) * GridCellSize), 0, 
                    Mathf.FloorToInt(Mathf.FloorToInt(hit.z / GridCellSize) * GridCellSize)) + _halfCellSizeVector;
            if (previewInstance != null)
                previewInstance.transform.position = gridPos;
            _cellVisualizer.transform.position = gridPos;
            return true;
        }

        /// <summary>
        /// Check if the preview tile is placed
        /// </summary>
        private bool CheckPlacement(Vector3 gridPos)
        {
            if (!Input.GetMouseButton(0) || DeleteMode || previewInstance == null)
                return false;

            if (_placedTiles.ContainsKey(gridPos))
                return false;

            var tmp = previewInstance;
            _placedTiles.Add(gridPos, previewInstance);
            previewInstance.gameObject.name = $"GridTile_[{(int)gridPos.x}|{(int)gridPos.x}]";
            previewInstance = UnityEngine.Object.Instantiate(previewBluePrint, _map.transform);
            previewInstance.CheckObstaclesOnPlaced();
            previewInstance.ApplyRotation(tmp.gridTileRotation);
            EventTileBuilt?.Invoke(tmp);
            return true;
        }

        /// <summary>
        /// Check if the current preview tile is rotated
        /// </summary>
        private bool CheckObjectRotate()
        {
            if (!Input.GetKeyDown(KeyCode.R) || DeleteMode || previewInstance == null) return false;
            previewInstance.Rotate();
            return true;
        }

        /// <summary>
        /// Check if a tile for the given position is delted
        /// </summary>
        private void CheckDelete(Vector3 gridPos)
        {
            if (!Input.GetMouseButton(0) || !DeleteMode) return;
               
            if(_placedTiles.TryGetValue(gridPos, out var go) && !go.MarkedToDelete)
            {
                go.MarkedToDelete = true;
                _placedTiles.Remove(gridPos);
                EventTileDeleted?.Invoke(go);
            }
        }


        public static string SavePath => $"{Application.persistentDataPath}" + "/map_{0}.sav";

        public static void TriggerSaveForPlayer(int playerId = 1)
        {
            if (_managers == null || _managers.Count <= 0) return;
            foreach (var manager in _managers)
            {
                if (manager.PlayerBuildModeManager.PlayerId.Equals(playerId))
                {
                    manager.GenerateSaveFile();
                    return;
                }
            }
        }

        public void GenerateSaveFile()
        {
            if (_placedTiles == null || _placedTiles.Count < 0) return;
            int index = 0;
            var path = string.Format(SavePath, PlayerBuildModeManager.PlayerId);
            using (StreamWriter writer = new StreamWriter(path))
            {
                foreach (var o in _placedTiles)
                {
                    writer.WriteLine(JsonUtility.ToJson(o.Value.ToSave()));
                    index++;
                }
            }
        }
    }
}


