#region Import

using System.Collections;
using UnityEngine;

#endregion

namespace Grid
{
    public class SelectionVisualizer : MonoBehaviour
    {
        /// <summary>
        /// The sprite when selecting
        /// </summary>
        [Tooltip("The sprite when selecting.")]
        public GameObject selectorSprite;

        /// <summary>
        /// The sprite when deleting
        /// </summary>
        [Tooltip("The sprite when deleting.")]
        public GameObject deleteSprite;

        private Coroutine _routine;

        private Vector3 _initialDeleteScale;

        private void Awake()
        {
            _initialDeleteScale = deleteSprite.transform.localScale;
            SetColorState(true);
        }

        public void SetColorState(bool normalMode)
        {
            selectorSprite.SetActive(normalMode);
            deleteSprite.SetActive(!normalMode);
            if (normalMode)
            {
                if (_routine != null)
                {
                    StopCoroutine(_routine);
                    _routine = null;
                }
            }
            else
            {
                deleteSprite.transform.localScale = _initialDeleteScale;
                _routine = StartCoroutine(Scale());
            }
        }

        private void OnDisable()
        {
            deleteSprite.transform.localScale = _initialDeleteScale;
            _routine = null;
        }

        private float animationTime = 0.50f;

        private IEnumerator Scale()
        {
            float tmp = 0.0f;
            bool getBigger = true;
            Vector3 startScale = deleteSprite.transform.localScale;
            Vector3 endScale = startScale * 1.25f;
            while (true)
            {
                tmp = Mathf.Min(animationTime, tmp + Time.deltaTime);
                deleteSprite.transform.localScale = getBigger ?
                    Vector3.Lerp(startScale, endScale, tmp / animationTime) :
                    Vector3.Lerp(endScale , startScale, tmp / animationTime);
                yield return null;
                if(tmp >= animationTime)
                {
                    getBigger = !getBigger;
                    tmp = 0.0f;
                }
            }
            _routine = null;
        }
    }
}


