namespace Grid.Tiles
{
    /// <summary>
    /// The value could represent the cost for a t
    /// </summary>
    public enum GridTileType
    {
        None,
        Street,
        Trap,
        Tower
    }
}
