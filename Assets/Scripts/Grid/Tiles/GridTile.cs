#region Import

using UnityEngine;

#endregion

namespace Grid.Tiles
{
    public class GridTile : MonoBehaviour
    {
        /// <summary>
        /// The type thos grid tile represenets
        /// </summary>
        [Tooltip("The type thos grid tile represenets.")]
        public GridTileType gridTileType = GridTileType.None;

        /// <summary>
        /// The current rotation of this tile
        /// </summary>
        [Tooltip("The current rotation of this tile.")]
        public GridTileRotation gridTileRotation = GridTileRotation.Degree0;

        /// <summary>
        /// The identifier of this grid tile
        /// </summary>
        [Tooltip("The identifier of this grid tile.")]
        public int identifier;

        /// <summary>
        /// The cost to build this tile
        /// </summary>
        [Tooltip("The cost to build this tile.")]
        public int cost = 0;

        /// <summary>
        /// The index of this grid tile on the grid
        /// </summary>
        [HideInInspector]
        public int[] localPos;

        /// <summary>
        /// The maximum amount of obstacles
        /// </summary>
        [Tooltip("The maximum amount of obstacles.")]
        public int maxObstacles = 2;
        
        /// <summary>
        /// The obstacle spawn positions
        /// </summary>
        [Tooltip("The obstacle spawn positions.")]
        public GridTileObstacle[] obstacleSpawnDefintions;

        /// <summary>
        /// Is this tile marked as deleted
        /// </summary>
        public bool MarkedToDelete { get; set; }

        /// <summary>
        /// Rotate this object
        /// </summary>
        public void Rotate()
        {
            int currRotation = (int)gridTileRotation + 1;
            if (currRotation > 3)
                currRotation = 0;
            gridTileRotation = (GridTileRotation)currRotation;
            transform.Rotate(new Vector3(0.0f, 90.0f, 0.0f));
        }

        public void CheckObstaclesOnPlaced()
        {
            if (obstacleSpawnDefintions == null || obstacleSpawnDefintions.Length < 0) return;
            int obstCount = 0;
            foreach (var obst in obstacleSpawnDefintions)
            {
                if (obst.ChooseRandomObstacle())
                    obstCount++;

                if (obstCount == maxObstacles)
                    return;
            } 
        }

        public void ApplyRotation(GridTileRotation rotation)
        {
            gridTileRotation = rotation;
            transform.localRotation = Quaternion.Euler(gridTileRotation.ToEuler());
        }

        public void ClearObstacles()
        {
            if (obstacleSpawnDefintions == null || obstacleSpawnDefintions.Length < 0) return;
            foreach (var obst in obstacleSpawnDefintions)
                obst.Clear();
        }

        public GridTileSaveFile ToSave()
        {
            var file = new GridTileSaveFile();
            file.identfier = identifier;
            var pos = transform.position;
            file.localPos = new int[] { (int)pos.x, (int)pos.z };
            file.rotation = (int)gridTileRotation;
            file.obstaclesIndices = new int[obstacleSpawnDefintions.Length];
            for (int i = 0; i < obstacleSpawnDefintions.Length; i++)
                file.obstaclesIndices[i] = obstacleSpawnDefintions[i].ObstacleIndex;
            return file;
        }

        public void Load(GridTileSaveFile file)
        {
            localPos = file.localPos;
            transform.localPosition = new Vector3(localPos[0], 0.0f, localPos[1]);
            ApplyRotation((GridTileRotation)file.rotation);
            if (obstacleSpawnDefintions == null || obstacleSpawnDefintions.Length < 0) return;
            for (int i = 0; i < obstacleSpawnDefintions.Length; i++)
                obstacleSpawnDefintions[i].SpawnSpecificObstacle(file.obstaclesIndices[i]);
        }
    }
}

