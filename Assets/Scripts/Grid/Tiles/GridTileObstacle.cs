#region Import 

using System;
using UnityEngine;

#endregion

namespace Grid.Tiles
{
    [Serializable]
    public class GridTileObstacle
    {
        /// <summary>
        /// The position where to spawn the obstacle
        /// </summary>
        [Tooltip("The position where to spawn the obstacle.")]
        public Transform obstaclePosition;

        /// <summary>
        /// All available obstacles
        /// </summary>
        [Tooltip("All available obstacles.")]
        public GameObject[] obstacles;

        [Range(0.0f,1.0f)]
        /// <summary>
        /// The chance to spawn an obstacle
        /// </summary>
        [Tooltip("The position where to spawn the obstacle.")]
        public float chanceToSpawn = 1.0f;

        /// <summary>
        /// The index of the current spawned obstacle
        /// </summary>
        public int ObstacleIndex { get; private set; } = -1;

        private bool SpawnNotValid => obstaclePosition == null || obstacles == null || obstacles.Length < 0;

        private GameObject _currentObstacle;

        public bool ChooseRandomObstacle()
        {
            if (SpawnNotValid) return false;
            var chance = (float)UnityEngine.Random.Range(1, 1000000);
            chance /= 1000000;
            if (chance > chanceToSpawn) return false;
            ObstacleIndex = UnityEngine.Random.Range(0, obstacles.Length);
            _currentObstacle = UnityEngine.Object.Instantiate(obstacles[ObstacleIndex], obstaclePosition.position, Quaternion.identity, obstaclePosition.transform);
            return true;
        }

        public void SpawnSpecificObstacle(int index)
        {
            if (SpawnNotValid || index < 0 || index >= obstacles.Length) return;
            ObstacleIndex = index;
            _currentObstacle = UnityEngine.Object.Instantiate(obstacles[ObstacleIndex], obstaclePosition.position, Quaternion.identity, obstaclePosition.transform);
        }

        public void Clear()
        {
            ObstacleIndex = -1;
            if(_currentObstacle != null)
                UnityEngine.Object.Destroy(_currentObstacle);
        }
    }
}
