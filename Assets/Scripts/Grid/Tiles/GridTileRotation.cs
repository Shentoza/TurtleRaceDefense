#region Import

using UnityEngine;

#endregion

namespace Grid.Tiles
{
    public enum GridTileRotation
    { 
        Degree0 = 0,
        Degree90 = 1,
        Degree180 = 2,
        Degree270 = 3
    }

    public static class GridTileRotationExtension
    {
        public static Vector3 ToEuler(this GridTileRotation rotation)
        {
            switch (rotation)
            {
                case GridTileRotation.Degree0:
                    return Vector3.zero;
                case GridTileRotation.Degree90:
                    return new Vector3(0.0f, 90.0f, 0.0f);
                case GridTileRotation.Degree180:
                    return new Vector3(0.0f, 180.0f, 0.0f);
                case GridTileRotation.Degree270:
                    return new Vector3(0.0f, 270.0f, 0.0f);
            }
            return Vector3.zero;
        }
    }
}
