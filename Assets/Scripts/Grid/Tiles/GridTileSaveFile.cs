#region Import

using System;

#endregion

namespace Grid.Tiles
{
    [Serializable]
    public class GridTileSaveFile
    {
        public int identfier;

        public int[] localPos;

        public int rotation;

        public int[] obstaclesIndices;

        public override string ToString()
        {
            var tmp = "";
            foreach (int i in obstaclesIndices)
                tmp += $"i,";
            return $"{identfier} | ({localPos[0]}|{localPos[1]}) | {(GridTileRotation)rotation} | ({tmp})";
        }
    }
}
