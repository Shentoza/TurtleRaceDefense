#region Import

using System;
using Grid.Tiles;
using UnityEngine;

#endregion

namespace Grid.Data
{
    [Serializable]
    public class TileCollection
    {
        /// <summary>
        /// The type of all tiles which reside in this entry
        /// </summary>
        [Tooltip("The type of all tiles which reside in this entry.")]
        public GridTileType gridTileType = GridTileType.None;

        /// <summary>
        /// All tiles
        /// </summary>
        [Tooltip("All tiles.")]
        public TileLibraryTileEntry[] tiles;
    }
}
