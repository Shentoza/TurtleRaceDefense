#region Import

using System;
using Grid.Tiles;
using UnityEngine;

#endregion

namespace Grid.Data
{
    [Serializable]
    public class TileTypeEntry
    {
        /// <summary>
        /// The type of all tiles which reside in this entry
        /// </summary>
        [Tooltip("The type of all tiles which reside in this entry.")]
        public GridTileType gridTileType = GridTileType.None;

        /// <summary>
        /// The sprite to represent this GridTileType
        /// </summary>
        [Tooltip("The sprite to represent this GridTileType.")]
        public Sprite sprite;
    }
}
