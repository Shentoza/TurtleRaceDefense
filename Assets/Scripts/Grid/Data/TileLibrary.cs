#region Import

using UnityEngine;

#endregion

namespace Grid.Data
{
    [CreateAssetMenu(fileName = "TileLibrary", menuName = "Grid/Data/TileLibrary")]
    public class TileLibrary : ScriptableObject
    {
        /// <summary>
        /// All grid types with their sprites
        /// </summary>
        public TileTypeEntry[] tileTypeEntries;


        /// <summary>
        /// All tiles which can be build by the player
        /// </summary>
        [Tooltip("All tiles which can be build by the player.")]
        public TileCollection[] tileCollections;
    }
}


