#region Import

using System;
using Grid.Tiles;
using UnityEngine;

#endregion

namespace Grid.Data
{
    [Serializable]
    public class TileLibraryTileEntry
    {
        /// <summary>
        /// The tile
        /// </summary>
        [Tooltip("The tile.")]
        public GridTile tile;

        /// <summary>
        /// The sprite representing this tile
        /// </summary>
        [Tooltip("The sprite representing this tile.")]
        public Sprite sprite;
    }
}
