#region Import

using UnityEngine;
using PlayerBuildMode;
using Cinemachine;

#endregion

namespace Grid.CameraMovement
{
    public class GridCamera
    {
        /// <summary>
        /// Is the user currently dragging?
        /// </summary>
        private bool _dragging;
   
        private PlayerBuildModeManager PlayerBuildModeManager { get; set; }

        private Camera Cam => PlayerBuildModeManager.playerCamera;

        private Vector3 _hitPosLastFrame;
        private Vector3 _hitPosThisFrame;
        private Vector3 _directionThisFrame;
        private Vector3 _mousePosLastFrame;

        private Vector3 _initalOffset;
        private Vector3 _maxZoom;
        private Vector3 _minZoom;
        private CinemachineTransposer _transposer;
        private float _currentZoom = 0.5f;
        private int _moveKeyIndex = 0;

        public void Initialize(PlayerBuildModeManager pBM)
        {
            PlayerBuildModeManager = pBM;
            var y = pBM.AreaDefinition.CellSize * 4;
            _initalOffset = new Vector3(0, y, -y);
            _minZoom = _initalOffset * (1.0f + PlayerBuildModeManager.cameraZoomPercentage);
            _maxZoom = _initalOffset * (1.0f - PlayerBuildModeManager.cameraZoomPercentage);
            _currentZoom = 0.5f;
            _transposer = PlayerBuildModeManager.virtualCamera.GetCinemachineComponent<CinemachineTransposer>();
            if(_transposer != null)
                _transposer.m_FollowOffset = _initalOffset;
        }

        public void OnBuildModeStarted()
        {
            _moveKeyIndex = 1;
            OnDragEnd();
        }

        public void OnBuildModeEnded()
        {
            _moveKeyIndex = 0;
            OnDragEnd();
        }

        public void DoUpdate()
        {
            var mouseWheel = Input.GetAxis("Mouse ScrollWheel");
            if (mouseWheel > 0 || mouseWheel < 0)
                OnZoom(mouseWheel);

            if (!_dragging && Input.GetMouseButtonDown(_moveKeyIndex))
                OnDragBegin();
            else if (_dragging && Input.GetMouseButtonUp(_moveKeyIndex))
                OnDragEnd();

            if (!_dragging) return;
            OnDrag();
        }

        private void OnDragBegin()
        {
            _dragging = true;
            _mousePosLastFrame = Input.mousePosition;
        }

        private void OnDragEnd()
        {
            _dragging = false;
            _hitPosLastFrame = _hitPosThisFrame = _directionThisFrame =  Vector3.zero;
        }

        private void OnDrag()
        {
            var mouseDistance = Vector3.Distance(_mousePosLastFrame, Input.mousePosition);
            if (mouseDistance < 0.1f)
            {
                return;
            }
            _mousePosLastFrame = Input.mousePosition;

            var ray = PlayerBuildModeManager.playerCamera.ScreenPointToRay(Input.mousePosition);
            if (!PlayerBuildModeManager.AreaDefinition.HasRayValidPosition(ray, out var hit))
            {
                _hitPosThisFrame = Vector3.zero;
                return;
            }

            var point = new Vector3(hit.x, 0, hit.z);
            _directionThisFrame = (_hitPosLastFrame - point);
            var distance = _directionThisFrame.sqrMagnitude;
            if (distance < 0.001f) return;
            _hitPosLastFrame = _hitPosThisFrame;
            _hitPosThisFrame = point;
            PlayerBuildModeManager.cameraTarget.transform.position += _directionThisFrame.normalized * Time.deltaTime * PlayerBuildModeManager.cameraMoveSpeed;
        }

        private void OnZoom(float zoom)
        {
            var t = Cam.transform;
            _currentZoom = Mathf.Clamp(_currentZoom + zoom, 0.0f, 1.0f);
            _transposer.m_FollowOffset = Vector3.Lerp(_minZoom, _maxZoom, _currentZoom);
        }
    }
}

