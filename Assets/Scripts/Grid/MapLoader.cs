#region Import

using UnityEngine;
using Grid.Data;
using Grid.Tiles;
using System.IO;
using System.Collections.Generic;

#endregion

namespace Grid
{
    public class MapLoader : MonoBehaviour
    {
        /// <summary>
        /// The tile library
        /// </summary>
        [Tooltip("The tile library.")]
        public TileLibrary tileLibrary;

        /// <summary>
        /// The id of the player to load the map for
        /// </summary>
        [Tooltip("The id of the player to load the map for.")]
        public int playerId = 1;

        private void Awake()
        {
            LoadMap(playerId);
        }

        public void LoadMap(int playerId)
        {
            var path = string.Format(GridManager.SavePath, playerId);
            if (!File.Exists(path))
            {
                Debug.LogWarning($"Couldn't find {path}");
                return;
            }

            var l = new List<GridTileSaveFile>();
            using (StreamReader reader = new StreamReader(path))
            {
                while (true)
                {
                    var line = reader.ReadLine();
                    if (line == null) break;
                    l.Add(JsonUtility.FromJson < GridTileSaveFile>(line));
                }
            }

            if(l.Count == 0 || tileLibrary == null || tileLibrary.tileCollections == null)
            {
                Debug.LogWarning($"No save files loaded ({path})");
                return;
            }

            foreach(var file in l)
            {
                GridTile tile = null;
                foreach(var collection in tileLibrary.tileCollections)
                {
                    if (collection.tiles == null) continue;
                    foreach(var gridTile in collection.tiles)
                    {
                        if (gridTile.tile.identifier.Equals(file.identfier))
                        {
                            tile = gridTile.tile;
                            break;
                        }
                    }
                    if (tile != null)
                        break;
                }

                if (tile == null) continue;
                var instance = Instantiate(tile, transform);
                instance.Load(file);
            }
        }
    }

}

