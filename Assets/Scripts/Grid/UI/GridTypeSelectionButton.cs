#region Import

using UnityEngine;
using UnityEngine.UI;
using System;
using Grid.Tiles;
using Grid.Data;

#endregion

namespace Grid.UI
{
    [RequireComponent(typeof(Button))]
    public class GridTypeSelectionButton : MonoBehaviour
    {
        /// <summary>
        /// The icon of this button
        /// </summary>
        [Tooltip("The icon of this button.")]
        public Image icon;

        /// <summary>
        /// The color to use to show that the button is selected
        /// </summary>
        [Tooltip("The color to use to show that the button is selected.")]
        public Color selectedColor = Color.green;

        /// <summary>
        /// The color to use to show that the button is not selected
        /// </summary>
        [Tooltip("The color to use to show that the button is not selected.")]
        public Color normalColor = Color.white;

        [HideInInspector]
        ///The GridTileType this button represents
        public GridTileType GridTileType {
            get {
                if (tileCollection == null)
                    return GridTileType.None;
                return tileCollection.gridTileType;
            }
        }

        /// <summary>
        /// Button cache
        /// </summary>
        private Button _button;

        /// <summary>
        /// On pressed event
        /// </summary>
        public event Action<GridTypeSelectionButton> EventGridTileCollectionChoosen;

        /// <summary>
        /// The selected state of the button
        /// </summary>
        public bool IsSelected { get; private set; }

        /// <summary>
        /// The tile collection of this button
        /// </summary>
        public TileCollection tileCollection { get; private set; }

        private void Awake()
        {
            if (!TryGetComponent(out _button)) return;
            _button.targetGraphic.color = normalColor;
            _button.onClick.AddListener(OnButtonPressed);
        }

        public void SetGridTileCollection(TileCollection collection)
        {
            tileCollection = collection;
        }
            
        public void SetSelectedState(bool selected)
        {
            _button.targetGraphic.color = selected ? selectedColor : normalColor;
            IsSelected = selected;
        }

        private void OnButtonPressed()
        {
            EventGridTileCollectionChoosen?.Invoke(this);
        }
    }
}
