#region Import

using UnityEngine;
using UnityEngine.UI;
using System;
using Grid.Data;

#endregion

namespace Grid.UI
{
    [RequireComponent(typeof(Button))]
    public class GridTileSelectionButton : MonoBehaviour
    {
        /// <summary>
        /// The icon of this button
        /// </summary>
        [Tooltip("The icon of this button.")]
        public Image icon;

        /// <summary>
        /// The color to use to show that the button is selected
        /// </summary>
        [Tooltip("The color to use to show that the button is selected.")]
        public Color selectedColor = Color.green;

        /// <summary>
        /// The color to use to show that the button is not selected
        /// </summary>
        [Tooltip("The color to use to show that the button is not selected.")]
        public Color normalColor = Color.white;

        [HideInInspector]
        ///The GridTileType this button represents
        public TileLibraryTileEntry tileLibraryTileEntry { get; private set; }

        /// <summary>
        /// Button cache
        /// </summary>
        private Button _button;

        /// <summary>
        /// On pressed event
        /// </summary>
        public event Action<GridTileSelectionButton> EventGridTileSelected;

        /// <summary>
        /// The selected state of the button
        /// </summary>
        public bool IsSelected { get; private set; }

        private void Awake()
        {
            if (!TryGetComponent(out _button)) return;
            _button.targetGraphic.color = normalColor;
            _button.onClick.AddListener(OnButtonPressed);
        }

        public void SetGridTile(TileLibraryTileEntry tileLibraryTileEntry)
        {
            this.tileLibraryTileEntry = tileLibraryTileEntry;
            icon.sprite = tileLibraryTileEntry.sprite;
        }

        public void SetSelectedState(bool selected)
        {
            _button.targetGraphic.color = selected ? selectedColor : normalColor;
            IsSelected = selected;
        }

        private void OnButtonPressed()
        {
            EventGridTileSelected?.Invoke(this);
        }
    }
}
