#region Import

using System.Collections.Generic;
using UnityEngine;
using System;
using Grid.Data;
using System.Linq;
using PlayerBuildMode;

#endregion

namespace Grid.UI
{
    public class GridToolbar
    {
        private List<GridTypeSelectionButton> _gridTypeButtons;

        private List<GridTileSelectionButton> _activeGridTileButtons;
        private List<GridTileSelectionButton> _inactiveGridTileButtons;
        
        public event Action<GridTileSelectionButton> EventGridTileSelected;

        public event Action EventButtonDeselected;

        private PlayerBuildModeManager PlayerBuildModeManager { get; set; }

        private TileLibrary _tileLibrary => PlayerBuildModeManager.tileLibrary;
        private GridTypeSelectionButton _gridTypeSelectionButton => PlayerBuildModeManager.gridTypeSelectionButton;
        private Transform _typeSelectionTransform => PlayerBuildModeManager.typeSelectionTransform;
        private GridTileSelectionButton _gridTileSelectionButton => PlayerBuildModeManager.gridTileSelectionButton;
        private Transform _tileSelectionTransform => PlayerBuildModeManager.tileSelectionTransform;

        public void Initialize(PlayerBuildModeManager pbM)
        {
            PlayerBuildModeManager = pbM;
            if (_tileLibrary.tileTypeEntries == null || _tileLibrary.tileTypeEntries.Length == 0 ||
                _tileLibrary.tileCollections == null || _tileLibrary.tileCollections.Length == 0)
            {
#if UNITY_EDITOR
                Debug.Log("No tile types or tile collections defined.");
#endif
                return;
            }

            _gridTypeButtons = new List<GridTypeSelectionButton>();
            _activeGridTileButtons = new List<GridTileSelectionButton>();
            _inactiveGridTileButtons = new List<GridTileSelectionButton>();
            foreach (var type in _tileLibrary.tileTypeEntries)
            {
                if (type.sprite == null)
                {
#if UNITY_EDITOR
                    Debug.Log($"No sprite defined for {type.gridTileType.ToString()}.");
#endif
                    continue;
                }

                var coll = _tileLibrary.tileCollections.FirstOrDefault(obj => obj.gridTileType == type.gridTileType);
                if (!(coll != null && coll.tiles != null && coll.tiles.Length > 0))
                {
#if UNITY_EDITOR
                    Debug.Log($"No tile collection for {type.gridTileType.ToString()} found. Or collection was empty.");
#endif
                    continue;
                }

                var b = UnityEngine.Object.Instantiate(_gridTypeSelectionButton, _typeSelectionTransform);
                b.icon.sprite = type.sprite;
                b.SetGridTileCollection(coll);
                b.name = $"{_gridTypeSelectionButton.name}_Button";
                b.EventGridTileCollectionChoosen -= OnEventGridTileCollectionChoosen;
                b.EventGridTileCollectionChoosen += OnEventGridTileCollectionChoosen;
                _gridTypeButtons.Add(b);
            }
        }

        private void OnEventGridTileCollectionChoosen(GridTypeSelectionButton button)
        {
            if (button.IsSelected)
            {
                EventButtonDeselected?.Invoke();
                HideAllGridTileSelectionButton();
                button.SetSelectedState(false);
                return;
            }

            foreach (var b in _gridTypeButtons)
                b.SetSelectedState(button == b);
            SpawnGridTileButtons(button.tileCollection);
        }

        private void HideAllGridTileSelectionButton()
        {
            if (_activeGridTileButtons.Count == 0) return;
            for(int i = _activeGridTileButtons.Count - 1; i >= 0; i--)
            {
                var b = _activeGridTileButtons[i];
                b.SetSelectedState(false);
                _inactiveGridTileButtons.Add(b);
                _activeGridTileButtons.RemoveAt(i);
                b.gameObject.SetActive(false);
            }
        }

        private void SpawnGridTileButtons(TileCollection tileCollection)
        {
            if (tileCollection.tiles == null || tileCollection.tiles.Length == 0) return;
            int diff = _activeGridTileButtons.Count - tileCollection.tiles.Length;
            if(diff < 0)
            {
                var abs = Mathf.Abs(diff);
                if (_inactiveGridTileButtons.Count > 0)
                {
                    for (int i = _inactiveGridTileButtons.Count - 1; i >= 0; i--)
                    {
                        var b = _inactiveGridTileButtons[i];
                        _activeGridTileButtons.Add(b);
                        _inactiveGridTileButtons.RemoveAt(i);
                        b.EventGridTileSelected -= OnEventGridTileSelected;
                        b.EventGridTileSelected += OnEventGridTileSelected;
                        b.gameObject.SetActive(true);
                        abs--;
                        if (abs <= 0)
                            break;
                    }
                }

                if (abs > 0)
                {
                    for (int count = 0; count < abs; count++)
                    {
                        var b = UnityEngine.Object.Instantiate(_gridTileSelectionButton, _tileSelectionTransform);
                        b.EventGridTileSelected -= OnEventGridTileSelected;
                        b.EventGridTileSelected += OnEventGridTileSelected;
                        _activeGridTileButtons.Add(b);
                    }
                }
            }

            int index = 0;
            for (int i = _activeGridTileButtons.Count - 1; i >= 0; i--)
            {
                var b = _activeGridTileButtons[i];
                if (i >= tileCollection.tiles.Length)
                {
                    b.EventGridTileSelected -= OnEventGridTileSelected;
                    if (b.IsSelected)
                        b.SetSelectedState(false);
                    _inactiveGridTileButtons.Add(b);
                    _activeGridTileButtons.RemoveAt(i);
                    b.gameObject.SetActive(false);
                    continue;
                }
                b.SetGridTile(tileCollection.tiles[index]);
                index++;
            }
        }

        private void OnEventGridTileSelected(GridTileSelectionButton gridTileButton)
        {
            if (gridTileButton.IsSelected)
            {
                gridTileButton.SetSelectedState(false);
                EventButtonDeselected?.Invoke();
                return;
            }

            foreach (var b in _activeGridTileButtons)
                b.SetSelectedState(gridTileButton == b);
            EventGridTileSelected?.Invoke(gridTileButton);
        }
    }
}
