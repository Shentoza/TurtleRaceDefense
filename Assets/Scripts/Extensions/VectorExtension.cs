using UnityEngine;

namespace Extensions
{
    public static class VectorExtensions
    {
        public static float MaxElement(this Vector3 vector)
        {
            return Mathf.Max(vector.x, vector.y, vector.z);
        }
    }
}
