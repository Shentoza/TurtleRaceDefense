#region Import

using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

#endregion

public class EditorPropertyBlock
{
    private SerializedObject _obj;

    private string[] _properties;

    public EditorPropertyBlock(SerializedObject obj, params string[] properties)
    {
        _obj = obj;
        _properties = properties;
    }

    public void DrawAndSave(Scene scene)
    {
        if (_obj == null || _properties == null || _properties.Length <= 0) return;
        EditorGUI.BeginChangeCheck();

        foreach(var propString in _properties)
        {
            var prop = _obj.FindProperty(propString);
            if (prop != null)
                EditorGUILayout.PropertyField(prop);
        }

        if (!EditorGUI.EndChangeCheck()) return;
        _obj.ApplyModifiedProperties();

        if (!Application.isPlaying)
            EditorSceneManager.MarkSceneDirty(scene);
    }
}
