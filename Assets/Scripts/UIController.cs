using System.Collections;
using System.Collections.Generic;
using GameFlow;
using Grid;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private int _playerID = 1;
    
    private IGameFlowManager _gameFlowManager;
    
    void Start()
    {
        _gameFlowManager = FindObjectOfType<GameFlowManager>();

        if (_gameFlowManager == null)
        {
            Debug.LogError("GameFlowManager not found in scene!");
        }
    }
    
    public void SaveMap()
    {
        Debug.Log("Save Map");
        GridManager.TriggerSaveForPlayer(_playerID);
    }
    
    public void StartRacingMode()
    {
        Debug.Log("Save Map");
        if (_gameFlowManager != null)
        {
            _gameFlowManager.ChangeToState(EGameFlowState.Racing);
        }
    }
}
