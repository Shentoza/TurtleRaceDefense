﻿using System.Collections.Generic;

namespace Mesh
{
    public interface ITournamentInitializer
    {
        public IReadOnlyList<TournamentParticipant> ProvideParticipants();
    }
}