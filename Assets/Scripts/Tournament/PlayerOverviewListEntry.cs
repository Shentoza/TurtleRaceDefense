using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mesh
{
    public class PlayerOverviewListEntry : MonoBehaviour
    {
        [SerializeField] private Text _name;

        public void Setup(string name)
        {
            _name.text = name;
        }
    }
}
