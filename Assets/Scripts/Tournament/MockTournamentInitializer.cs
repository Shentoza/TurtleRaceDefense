﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Mesh
{
    public class MockTournamentInitializer : MonoBehaviour, ITournamentInitializer
    {
        [SerializeField] private int _numPlayers = 4;
        
        public IReadOnlyList<TournamentParticipant> ProvideParticipants()
        {
            List<TournamentParticipant> tournamentParticipants = new List<TournamentParticipant>();
            
            for (int i = 0; i < _numPlayers; i++)
            {
                TournamentParticipant participant = new TournamentParticipant();
                participant.Setup("Mock Player Name " + i);
                tournamentParticipants.Add(participant);
            }

            return tournamentParticipants;
        }

        // private void Start()
        // {
        //     _tournamentManager.Setup(this, null);
        // }
    }
}