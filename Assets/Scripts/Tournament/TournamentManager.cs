using System;
using System.Collections;
using System.Collections.Generic;
using Services;
using UnityEngine;

namespace Mesh
{
    public class TournamentManager : MonoBehaviour
    {
        // [SerializeField] private 
        // load next round
        // save maps
        // participants

        // timer per round

        [SerializeField] private PlayerOverviewListEntry _prefabPlayerOverviewListEntry;
        [SerializeField] private Transform _playerOverviewParent;

        private List<TournamentParticipant> _tournamentParticipants;

        public int NumParticipants => _tournamentParticipants.Count;

        public void Setup(ITournamentInitializer initializer, IEventService eventService)
        {
            _tournamentParticipants = new List<TournamentParticipant>();
            foreach (TournamentParticipant tournamentParticipant in initializer.ProvideParticipants())
            {
                _tournamentParticipants.Add((TournamentParticipant) tournamentParticipant.Clone());
            }

            foreach (TournamentParticipant participant in _tournamentParticipants)
            {
                var playerName = Instantiate(_prefabPlayerOverviewListEntry, _playerOverviewParent) as PlayerOverviewListEntry;
                playerName.Setup(participant.Name);
            }
            
            eventService.SubscribeEvent<GameStartedEvent>(OnGameStarted);
        }

        public void Shutdown()
        {
            
        }

        private void OnGameStarted(GameStartedEvent gameStartedEvent)
        {
            Debug.Log("TournamentManager: Game started! Message: " + gameStartedEvent.Message);
        }
    }
}