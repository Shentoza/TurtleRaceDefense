﻿using System;

namespace Mesh
{
    public class TournamentParticipant : ICloneable
    {
        public string Name => _name;
        
        private string _name;

        public void Setup(string name)
        {
            _name = name;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}