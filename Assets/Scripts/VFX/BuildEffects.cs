using System.Collections;
using System.Collections.Generic;
using Grid.Tiles;
using PlayerBuildMode;
using UnityEngine;

public class BuildEffects : MonoBehaviour
{
    private PlayerBuildModeManager BuildManager;

    [SerializeField]
    private ParticleSystem buildParticles;
    // Start is called before the first frame update
    void Start()
    {
        BuildManager = gameObject.GetComponent<PlayerBuildModeManager>();
        PlayerBuildModeManager.EventTileBuilt += OnTileBuilt;
    }

    private void OnDestroy()
    {
        PlayerBuildModeManager.EventTileBuilt -= OnTileBuilt;
    }

    void OnTileBuilt(int PlayerId, GridTile BuiltTile)
    {
        if (PlayerId == BuildManager.playerId)
        {
            if (buildParticles)
            {
                if (buildParticles.isPlaying)
                {
                    buildParticles.Stop();
                }

                buildParticles.transform.position = BuiltTile.transform.position;
                buildParticles.transform.localScale = Vector3.one * BuildManager.cellSize;
                buildParticles.Play();
            }
        }
    }
}
