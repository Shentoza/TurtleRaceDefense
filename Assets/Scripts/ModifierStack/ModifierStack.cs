using System.Collections.Generic;
using UnityEngine;

namespace ModifierStack
{
    public class ModifierStackValue
    {
        internal readonly GameObject Source;
        internal float BaseValue;

        public ModifierStackValue(float baseValue, GameObject source = null)
        {
            Source = source;
            this.BaseValue = baseValue;
        }

        public void SetValue(float newValue)
        {
            BaseValue = newValue;
        }

        public float GetCurrentValue()
        {
            return BaseValue;
        }
    }

    public class ModifierStack
    {
        private float _baseValue = 1.0F;
        private HashSet<ModifierStackValue> _internalStack;

        public ModifierStack(float baseValue)
        {
            this._baseValue = baseValue;
            _internalStack = new HashSet<ModifierStackValue>();
        }

        public void PushModifier(ModifierStackValue newModifier)
        {
            _internalStack.Add(newModifier);
        }

        public void RemoveModifier(ModifierStackValue modifier)
        {
            _internalStack.Remove(modifier);
        }

        public void RemoveAllModifiersFromSource(GameObject source)
        {
            _internalStack.RemoveWhere(value => value.Source == source);
        }

        public float GetTotalMagnitude()
        {
            float value = _baseValue;
            foreach (ModifierStackValue modifierStackValue in _internalStack)
            {
                value *= modifierStackValue.BaseValue;
            }
            return value;
        }

        public int NumEffects()
        {
            return _internalStack.Count;
        }
    }
}
