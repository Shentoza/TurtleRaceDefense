using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoard : MonoBehaviour
{
    public float boost = 500;

    public float jumpAmount = 3000;

    public AudioClip clip;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Entered SpeedBoard " + other.gameObject.name);

            if(clip != null)
            {
                AudioSource.PlayClipAtPoint(clip, other.transform.position);
            }

            Rigidbody rb = other.GetComponent<Rigidbody>();

            var force = other.transform.forward * boost;
            rb.AddForce(force, ForceMode.Acceleration);

            var jumpForce = other.transform.up * jumpAmount;
            var pos = other.GetComponent<CarController>().jumpAnchor.position;
            rb.AddForceAtPosition(jumpForce, pos, ForceMode.Impulse);
        }
    }
}
