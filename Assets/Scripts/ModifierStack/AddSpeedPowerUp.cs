using KartGame.KartSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddSpeedPowerUp : MonoBehaviour
{
    public float duration = 2f;
    public float addedAcceleration = 7f;
    public float addedTopSpeed = 15f;

    public AudioClip clip;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<KartGame.KartSystems.ArcadeKart>().AddPowerup(new ArcadeKart.StatPowerup(duration, addedAcceleration, addedTopSpeed));

            if (clip != null)
            {
                AudioSource.PlayClipAtPoint(clip, other.transform.position);
            }

            Destroy(gameObject);
        }
    }
}
