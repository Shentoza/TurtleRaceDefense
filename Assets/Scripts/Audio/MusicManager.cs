using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField] protected AudioClip raceMusic;
    [SerializeField] protected AudioClip buildMusic;
    [SerializeField] protected AudioClip menuMusic;
    [SerializeField] protected AudioSource audioSource;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void PlayRaceMusic()
    {
        if (raceMusic is null) return;
        audioSource.clip = raceMusic;
        audioSource.loop = true;
        audioSource.Play();
    }

    public void PlayBuildMusic()
    {
        if (buildMusic is null) return;
        audioSource.clip = buildMusic;
        audioSource.loop = true;
        audioSource.Play();
    }

    public void PlayMenuMusic()
    {
        if (menuMusic is null) return;
        audioSource.clip = menuMusic;
        audioSource.loop = true;
        audioSource.Play();
    }
    public void StopMusic()
    {
        audioSource.Pause();
    }
}
