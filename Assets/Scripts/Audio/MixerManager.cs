using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
public class MixerManager : MonoBehaviour
{

    [SerializeField] protected AudioMixer mainMixer;

    public AudioMixer MainMixer
    {
        get => mainMixer;
        set => mainMixer = value;
    }
    
    public void ChangeMasterLevel(float level)
    {
        SetLevel("Master", level);
    }

    public void ChangeSfxLevel(float level)
    {
        SetLevel("SFX", level);
    }

    public void ChangeAnnouncerLevel(float level)
    {
        SetLevel("Announcer", level);
    }

    public void ChangeMusicLevel(float level)
    {
        SetLevel("Music", level);
    }
    
    private void SetLevel(string mixerName, float level)
    {
        mainMixer.SetFloat(mixerName, level);
    }
}
