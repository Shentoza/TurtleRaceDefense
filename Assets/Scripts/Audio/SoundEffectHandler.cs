using UnityEngine;
using UnityEngine.Audio;

public class SoundEffectHandler : MonoBehaviour
{
    public enum MixerGroupSelection
    {
        SFX,
        Announcer,
        Music,
    }

    [SerializeField] protected AudioSource audioSource;
    [SerializeField] protected AudioMixer mainMixer;
    [SerializeField] protected AudioClip[] audioClips;
    [SerializeField] protected bool playDirectly = false;
    [SerializeField] protected MixerGroupSelection mixGroup = MixerGroupSelection.SFX;
    [SerializeField] protected bool spatialSound = true;
    [SerializeField] [Range(0, 255)] protected int priority = 128;

    [SerializeField][MinMax(-.5F,.5F, ShowEditRange = true)]
    public Vector2 volumeRange = new Vector2(-.15F, .1F);
    [SerializeField][MinMax(-.5F,.5F, ShowEditRange = true)]
    public Vector2 pitchRange = new Vector2(-.1F, .15F);
    
    private float _DefaultPitch;
    private float _defaultVolume;

    private void Start()
    {
        AudioSourceSetup();
        _DefaultPitch = audioSource.pitch;
        _defaultVolume = audioSource.volume;
        if (playDirectly)
            PlaySoundOneShot();
            
        
    }

    public void PlaySoundOneShot()
    {
        var usedPitch = _DefaultPitch + Random.Range(pitchRange.x, pitchRange.y);
        audioSource.pitch = usedPitch;
        var usedVolume = _defaultVolume + Random.Range(volumeRange.x, volumeRange.y);
        audioSource.volume = usedVolume;

        var clipIndex = Random.Range(0, audioClips.Length - 1);
        
        audioSource.PlayOneShot(audioClips[clipIndex]);
    }

    private void AudioSourceSetup()
    {
        switch (mixGroup)
        {
            case MixerGroupSelection.SFX:
                audioSource.outputAudioMixerGroup = mainMixer.FindMatchingGroups("Master/SFX")[0];
                break;
            case MixerGroupSelection.Announcer:
                audioSource.outputAudioMixerGroup = mainMixer.FindMatchingGroups("Master/Announcer")[0];
                break;
            case MixerGroupSelection.Music:
                audioSource.outputAudioMixerGroup = mainMixer.FindMatchingGroups("Master/Music")[0];
                break;
        }

        audioSource.priority = priority;
        audioSource.spatialBlend = spatialSound ? 1 : 0;
        
    }
}
