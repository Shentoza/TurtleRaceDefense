using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ModifierStack;

public class CarController : MonoBehaviour
{
    private const string HORIZONTAL = "Horizontal";
    private const string VERTICAL = "Vertical";
    private const string SLOWDEFAULTGROUND= "SlowDefaultGround";
    private const string SPEEDYGROUND = "SpeedyGround";

    private float horizontalInput;
    private float verticalInput;
    private float currentSteerAngle;
    private float currentBreakForce;
    private bool isBreaking;
    private float defaultSpeedModifier = 1;
    [SerializeField] private float speedModifier;
    // distance to ground for raycast to ground
    private int reachDown = 1;
    RaycastHit hit;

    public Transform jumpAnchor;

    private float defaultMotorforce = 1000;
    [SerializeField] private float motorForce;
    [SerializeField] private float breakForce;
    [SerializeField] private float maxSteerAngle;
    [SerializeField] private float slowGroundModifier = 0.5f;
    [SerializeField] private float speedGroundModifier = 1.5f;

    [SerializeField] private WheelCollider frontLeftWheelCollider;
    [SerializeField] private WheelCollider frontRightWheelCollider;
    [SerializeField] private WheelCollider rearLeftWheelCollider;
    [SerializeField] private WheelCollider rearRightWheelCollider;

    public ModifierStack.ModifierStack SpeedModifierStack = new ModifierStack.ModifierStack(1.0F);

    private ModifierStackValue GroundSpeedModifier = new ModifierStackValue(1.0F);

    private void Start()
    {
        SpeedModifierStack.PushModifier(GroundSpeedModifier);
    }

    private void FixedUpdate()
    {
        GetInput();
        HandleGroundSpeedModifiers();
        HandleMotor();
        HandleSteering();
    }

    private void GetInput()
    {
        horizontalInput = Input.GetAxis(HORIZONTAL);
        verticalInput= Input.GetAxis(VERTICAL);
    }

    private void HandleMotor()
    {
        //reset modifier
        speedModifier = SpeedModifierStack.GetTotalMagnitude();
        motorForce = defaultMotorforce * speedModifier;
        frontLeftWheelCollider.motorTorque = verticalInput * motorForce;
        frontRightWheelCollider.motorTorque = verticalInput * motorForce;
        currentBreakForce = isBreaking ? breakForce : 0f;
    }

    private void ApplyBreaking()
    {
        frontRightWheelCollider.brakeTorque = currentBreakForce;
        frontLeftWheelCollider.brakeTorque = currentBreakForce;
        rearLeftWheelCollider.brakeTorque = currentBreakForce;
        rearRightWheelCollider.brakeTorque = currentBreakForce;
    }

    private void HandleSteering()
    {
        currentSteerAngle = maxSteerAngle * horizontalInput;
        frontLeftWheelCollider.steerAngle = currentSteerAngle;
        frontRightWheelCollider.steerAngle = currentSteerAngle;
    }

    private void HandleGroundSpeedModifiers()
    {
        
        if(Physics.Raycast(transform.position, Vector3.down, out hit, reachDown))
        {
            if (hit.transform.tag == SLOWDEFAULTGROUND)
            {
                GroundSpeedModifier.SetValue(slowGroundModifier);
                //Debug.Log("On ground: " + SLOWDEFAULTGROUND);
            } 
            else if (hit.transform.tag == SPEEDYGROUND)
            {
                GroundSpeedModifier.SetValue(speedGroundModifier);

                //Debug.Log("On ground: " + SPEEDYGROUND);
            } 
            else
            {
                GroundSpeedModifier.SetValue(defaultSpeedModifier);
                //Debug.Log("On ground: " + "Default");
            }
        }

        //Debug.Log("Speed" + motorForce);
        
    }
}
