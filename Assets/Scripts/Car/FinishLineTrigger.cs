using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLineTrigger : MonoBehaviour
{
    public GameObject finishEffect;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Finish Line Entered! Do Stuff" + other.gameObject.name);

            if (finishEffect != null)
            {
                // Show effect
                Instantiate(finishEffect, other.transform.position, other.transform.rotation);
            }
            else
            {
                Debug.Log("No Finish Effect added");
            }

            //TODO Gameflow Logic
        }
    }
}
