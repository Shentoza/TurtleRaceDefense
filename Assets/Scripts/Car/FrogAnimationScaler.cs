using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogAnimationScaler : MonoBehaviour
{

    [SerializeField] private CarController _car;
    [SerializeField]  AnimationCurve _speedCurve;
    
    private Animator _animator;
    private Rigidbody _rigidbody;
    private static readonly int FSpeed = Animator.StringToHash("fSpeed");

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();

        _rigidbody = _car.GetComponent<Rigidbody>();
    }

    Plane _speedPlane = new Plane(new Vector3(0, 1, 0), 0);
    
    // Update is called once per frame
    void Update()
    {
        float speed = _speedPlane.ClosestPointOnPlane(_rigidbody.velocity).magnitude;
        Debug.Log(speed);
        _animator.SetFloat(FSpeed, _speedCurve.Evaluate(speed));

    }
}
