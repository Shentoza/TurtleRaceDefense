#region Import 

using Grid;
using Grid.Tiles;
using Grid.CameraMovement;
using UnityEngine;
using System;
using System.Collections;
using Cinemachine;
using Grid.Data;
using Grid.UI;

#endregion

namespace PlayerBuildMode
{
    public class PlayerBuildModeManager : MonoBehaviour
    {
        /// <summary>
        /// A reference to the camera of the player
        /// </summary>
        public Camera playerCamera;

        /// <summary>
        /// The id of the player
        /// </summary>
        [Tooltip("The id of the player.")]
        public int playerId = 1;

        #region Grid

        /// <summary>
        /// The size of the player grid in cells
        /// </summary>
        [Tooltip("Length and width of the grid.")]
        public int gridSizeInCells = 100;

        /// <summary>
        /// The size of one cell
        /// </summary>
        [Tooltip("The size of one cell.")]
        public float cellSize = 1.0f;

        /// <summary>
        /// An empty cell in the grid
        /// </summary>
        [Tooltip("An empty cell in the grid.")]
        public GameObject emptyCell;

        /// <summary>
        /// A gameObject to visualize the current cell
        /// </summary>
        [Tooltip("A gameObject to visualize the current cell.")]
        public SelectionVisualizer cellVisualizer;

        #endregion

        #region Camera

        /// <summary>
        /// The speed the camera is moved with
        /// </summary>
        [Tooltip("The speed the camera is moved with.")]
        public float cameraMoveSpeed = 20.0f;

        /// <summary>
        /// Zoom speed
        /// </summary>
        [Tooltip("Zoom speed.")]
        public float cameraZoomSpeed = 20.0f;

        /// <summary>
        /// Zoom percentage value.
        /// </summary>
        [Tooltip("Zoom percentage value.")]
        public float cameraZoomPercentage = 0.25f;

        /// <summary>
        /// The target for the camera.
        /// </summary>
        [Tooltip("The target for the camera.")]
        public GameObject cameraTarget;

        /// <summary>
        /// The virtual camera
        /// </summary>
        [Tooltip("The virtual camera.")]
        public CinemachineVirtualCamera virtualCamera;

        #endregion

        #region UI

        /// <summary>
        /// The tile library
        /// </summary>
        [Tooltip("The tile library.")]
        public TileLibrary tileLibrary;

        /// <summary>
        /// The prefab to use to spawn toolbar buttons for grid tile type selection
        /// </summary>
        [Tooltip("The prefab to use to spawn toolbar buttons for grid tile type selection.")]
        public GridTypeSelectionButton gridTypeSelectionButton;

        /// <summary>
        /// The prefab to use to spawn toolbar buttons for grid tile type selection
        /// </summary>
        [Tooltip("The prefab to use to spawn toolbar buttons for grid tile type selection.")]
        public Transform typeSelectionTransform;

        /// <summary>
        /// The prefab to use to spawn toolbar buttons for grid tile selection
        /// </summary>
        [Tooltip("The prefab to use to spawn toolbar buttons for grid tile selection.")]
        public GridTileSelectionButton gridTileSelectionButton;

        /// <summary>
        /// The prefab to use to spawn toolbar buttons for grid tile selection
        /// </summary>
        [Tooltip("The prefab to use to spawn toolbar buttons for grid tile selection.")]
        public Transform tileSelectionTransform;

        #endregion

        /// <summary>
        /// The id of the player
        /// </summary>
        public int PlayerId { get; private set; } = 1;

        /// <summary>
        /// The defined area for this player build mode
        /// </summary>
        public PlayerBuildAreaDefinition AreaDefinition { get; private set; }

        /// <summary>
        /// The grid manager
        /// </summary>
        private GridManager _gridManager;

        /// <summary>
        /// The grid camera
        /// </summary>
        private GridCamera _gridCamera;

        /// <summary>
        /// The building toolbar
        /// </summary>
        private GridToolbar _toolBar;
        
        public static event Action<int, GridTile> EventTileBuilt;

        public static event Action<int, GridTile> EventTileDeleted;

        private void Awake()
        {
            Initialize(playerId);
        }

        private void OnDestroy()
        {
            _gridManager?.CleanUp();
        }

        /// <summary>
        /// Initialize this player build mode with a player id
        /// </summary>
        public void Initialize(int playerId = 1)
        {
            PlayerId = playerId;
            if (gridSizeInCells % 2 != 0)
                gridSizeInCells++;
            AreaDefinition = new PlayerBuildAreaDefinition(transform.position, gridSizeInCells, cellSize);
            
            _gridManager = new GridManager();
            _gridManager.Initialize(this, emptyCell, cellVisualizer);
            _gridManager.EventTileBuilt += OnTileBuilt;
            _gridManager.EventTileDeleted += OnTileDeleted;

            _gridCamera = new GridCamera();
            _gridCamera.Initialize(this);

            _toolBar = new GridToolbar();
            _toolBar.Initialize(this);
            _toolBar.EventGridTileSelected += OnEventGridTileSelected;
            _toolBar.EventButtonDeselected += OnEventButtonDeselected;
        }

        private void Update()
        {
            _gridManager?.DoUpdate();
            _gridCamera?.DoUpdate();
        }

        private void OnTileBuilt(GridTile tile) => EventTileBuilt?.Invoke(PlayerId, tile);
      
        private void OnTileDeleted(GridTile tile)
        {
            EventTileDeleted?.Invoke(PlayerId, tile);
            StartCoroutine(DeleteTile(tile.gameObject));
        }

        private void OnEventGridTileSelected(GridTileSelectionButton selectionButton)
        {
            _gridManager.OnEventButtonSelected(selectionButton);
            _gridCamera.OnBuildModeStarted();
        }

        private void OnEventButtonDeselected()
        {
            _gridManager.OnEventButtonDeselected();
            _gridCamera.OnBuildModeEnded();
        }

        private IEnumerator DeleteTile(GameObject tile)
        {
            yield return null;
            Destroy(tile);
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(transform.position, new Vector3(gridSizeInCells * cellSize, 1.0f, gridSizeInCells * cellSize));
        }
#endif
    }
}