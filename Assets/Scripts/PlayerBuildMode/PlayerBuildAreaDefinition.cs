#region Import

using UnityEngine;

#endregion

namespace PlayerBuildMode
{
    public class PlayerBuildAreaDefinition
    {
        /// <summary>
        /// The plane defining the build plane of a player
        /// </summary>
        public Plane definingPlane;

        /// <summary>
        /// The bounds defining the valid build area of a player
        /// </summary>
        public Bounds definingBounds;
        
        /// <summary>
        /// The size of the player grid in cells
        /// </summary>
        public int GridSizeInCells { get; private set; }

        public float CellSize { get; private set; }

        public PlayerBuildAreaDefinition(Vector3 center, int gridSizeInCells, float cellSize)
        {
            definingPlane = new Plane(Vector3.up, center);
            definingBounds = new Bounds(center, new Vector3(gridSizeInCells * cellSize, 1.0f, gridSizeInCells * cellSize));
            GridSizeInCells = gridSizeInCells;
            CellSize = cellSize;
        }

        /// <summary>
        /// Check if a valid position inside the player bounds was found
        /// </summary>
        public bool HasRayValidPosition(Ray ray,out Vector3 hitPosition)
        {
            if (!definingPlane.Raycast(ray, out var hit))
            {
                hitPosition = Vector3.zero;
                return false;
            }

            hitPosition = ray.GetPoint(hit);
            return definingBounds.Contains(hitPosition);
        }
    }
}


