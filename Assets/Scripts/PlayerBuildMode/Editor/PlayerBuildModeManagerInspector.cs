#region Import

using UnityEngine;

#endregion

namespace PlayerBuildMode.Editor
{
    using UnityEditor;

    [CustomEditor(typeof(PlayerBuildModeManager))]
    public class PlayerBuildModeManagerInspector : Editor
    {
        private string[] _tabNames = new string[] { "General", "GridSettings", "CameraSettings", "UiSettings" };
        private int _sceletedTab = 0;
        
        private EditorPropertyBlock _generalProperties;
        private EditorPropertyBlock GeneralProperties {
            get {
                if(_generalProperties == null)
                    _generalProperties = new EditorPropertyBlock(serializedObject, "playerCamera", "playerId");
                return _generalProperties;
            }
        }

        private EditorPropertyBlock _gridSettingsProperties;
        private EditorPropertyBlock GridSettingsProperties {
            get {
                if (_gridSettingsProperties == null)
                    _gridSettingsProperties = new EditorPropertyBlock(serializedObject, "gridSizeInCells", "cellSize", "emptyCell", "cellVisualizer");
                return _gridSettingsProperties;
            }
        }

        private EditorPropertyBlock _cameraSettingsProperties;
        private EditorPropertyBlock CameraSettingsProperties {
            get {
                if (_cameraSettingsProperties == null)
                    _cameraSettingsProperties = new EditorPropertyBlock(serializedObject, "cameraTarget", "virtualCamera","cameraMoveSpeed", "cameraZoomPercentage");
                return _cameraSettingsProperties;
            }
        }

        private EditorPropertyBlock _uiSettingsProperties;
        private EditorPropertyBlock UiSettingsProperties {
            get {
                if (_uiSettingsProperties == null)
                    _uiSettingsProperties = new EditorPropertyBlock(serializedObject,"tileLibrary","gridTypeSelectionButton","typeSelectionTransform","gridTileSelectionButton","tileSelectionTransform");
                return _uiSettingsProperties;
            }
        }

        private PlayerBuildModeManager _playerBuildModeManager;

        private void OnEnable()
        {
            _sceletedTab = 0;
            _playerBuildModeManager = ((PlayerBuildModeManager)target);
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.Space(1);
            _sceletedTab = GUILayout.SelectionGrid(_sceletedTab, _tabNames, _tabNames.Length);
            switch(_sceletedTab)
            {
                case 0:
                    GeneralProperties.DrawAndSave(_playerBuildModeManager.gameObject.scene);
                    break;
                case 1:
                    GridSettingsProperties.DrawAndSave(_playerBuildModeManager.gameObject.scene);
                    break;
                case 2:
                    CameraSettingsProperties.DrawAndSave(_playerBuildModeManager.gameObject.scene);
                    break;
                case 3:
                    UiSettingsProperties.DrawAndSave(_playerBuildModeManager.gameObject.scene);
                    break;
            }
            EditorGUILayout.Space(1);
            EditorGUILayout.EndVertical();
        }
    }
}

