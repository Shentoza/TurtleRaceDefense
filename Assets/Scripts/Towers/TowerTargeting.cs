using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Towers
{
    public class TowerTargeting : MonoBehaviour
    {
        private TowerTargetComponent[] _availableTargets;

        [SerializeField] private CannonComponent _cannonComponent;
        [SerializeField] private TowerTargetComponent _target;
        
        public Quaternion _currentRotationalChange = Quaternion.identity;

        private void OnTriggerEnter(Collider other)
        {
            bool isPlayer = other.gameObject.CompareTag("Player");
            if (isPlayer)
            {
                _cannonComponent.AimAt(_target);
            }
        }

        private void OnDrawGizmos()
        {
            Color y = Color.yellow;
            y.a = 0.3F;
            Gizmos.color = y;
            Gizmos.DrawSphere(transform.position, GetComponent<SphereCollider>().radius);
        }
    }
}