using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Towers
{
    public class Projectile : MonoBehaviour
    {
        public bool VelocityLocalSpace = true;
        public Vector3 Velocity = new Vector3(0, 0, 100);

        private Rigidbody _rigidbody;

        // Start is called before the first frame update
        void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            SetVelocity(Velocity, VelocityLocalSpace);
        }

        public void SetVelocity(Vector3 velocity, bool isLocalSpace = true)
        {
            Velocity = velocity;
            VelocityLocalSpace = isLocalSpace;
            
            if (!_rigidbody)
            {
                _rigidbody = GetComponent<Rigidbody>();
            }
            if (isLocalSpace)
            {
                _rigidbody.velocity = transform.rotation * Velocity;
            }
            else
            {
                _rigidbody.velocity = Velocity;
            }
        }
    }
}