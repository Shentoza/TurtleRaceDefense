﻿// Copyright(C) battyRabbit UG (limited liability). All Rights Reserved.

using System;
using System.Collections;
using UnityEngine;

namespace Towers
{
    public class CannonComponent : MonoBehaviour
    {
        [Tooltip("Speed in degree per second")] [SerializeField]
        private float Turnspeed = 10F;

        private TowerTargetComponent _currentTarget;

        private float ShootingAngleInDegrees = 5.0F;

        [SerializeField] private GameObject ProjectilePrefab;
        [SerializeField] private Transform projectileSpawnAnchor;
        
        public void AimAt(TowerTargetComponent Target)
        {
            _currentTarget = Target;

            if (_currentTarget)
            {
                StartCoroutine(AimAtTarget(_currentTarget));
            }
        }

        private IEnumerator AimAtTarget(TowerTargetComponent currentTarget)
        {
            if (!currentTarget)
            {
                yield break;
            }

            var selfPosition = transform.position;
            
            Vector3 targetPlaneCannonPlane = _currentTarget.transform.position;
            targetPlaneCannonPlane.y = selfPosition.y;
            
            Vector3 TargetDir = (targetPlaneCannonPlane - selfPosition).normalized;
            Quaternion beginOrientation = transform.rotation;

            float cosShootingAngle = Mathf.Cos(Mathf.Deg2Rad * ShootingAngleInDegrees);

            float aimedAngle = Vector3.Dot(TargetDir, transform.forward);

            while (currentTarget && aimedAngle < cosShootingAngle)
            {
                Quaternion targetRotationWS =
                    Quaternion.LookRotation(targetPlaneCannonPlane - transform.position, Vector3.up);
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotationWS, Time.deltaTime * Turnspeed);
                yield return null;
                aimedAngle = Vector3.Dot(TargetDir, transform.forward);

            }

            float fwdVelocity = ((projectileSpawnAnchor.position - targetPlaneCannonPlane).magnitude);
            Debug.Log(fwdVelocity);
            
            ShootProjectile(Vector3.forward * fwdVelocity);
        }

        private void ShootProjectile(Vector3 localVelocity)
        {
            var projectileGO = GameObject.Instantiate(ProjectilePrefab, projectileSpawnAnchor.position, projectileSpawnAnchor.rotation);
            projectileGO.GetComponent<Projectile>().SetVelocity(localVelocity);
        }

        private void OnDrawGizmos()
        {
            if (_currentTarget)
            {
                Color r = Color.red;
                r.a = 0.3F;
                Gizmos.color = r;
                Gizmos.DrawSphere(_currentTarget.transform.position, .50F);
            }
        }
    }
}