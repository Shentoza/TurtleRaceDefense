using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{

    public float delay = 3f;

    public GameObject explosionEffect;

    float countdown;
    bool hasExploded = false;
    [SerializeField] private float explosionRadius = 10f;
    [SerializeField] private float explosionForce = 100000f;

    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if(countdown <= 0f && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }

    }

    private void Explode()
    {
        Debug.Log("BOOM!");
        if(explosionEffect != null)
        {
            // Show effect
            Instantiate(explosionEffect, transform.position, transform.rotation);

        } else
        {
            Debug.Log("No Explosion Effect added");
        }

        // Get nearby objects
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        // Add force
        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);
                Debug.Log("Got RB " + nearbyObject.gameObject.name);
            }
        }
        // Damage?

        // Remove Grenade
        Destroy(gameObject);
    }
}
