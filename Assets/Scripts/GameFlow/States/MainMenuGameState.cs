﻿using Mesh;
using Services;
using UnityEngine;

namespace GameFlow.States
{
    public class MainMenuGameState: MonoBehaviour, IGameFlowState
    {
        [SerializeField] private TournamentManager _tournamentManager;
        private IEventService _eventService;

        public void Setup(IEventService eventService)
        {
            _eventService = eventService;
            MockTournamentInitializer initializer = new MockTournamentInitializer();
            _tournamentManager.Setup(initializer, eventService);
            
            eventService.SubscribeEvent<GameStartedEvent>(OnGameStarted);
        }

        private void OnGameStarted(GameStartedEvent e)
        {
            Debug.Log("Main Menu Game Started!");
        }

        public void Enter()
        {
            Debug.Log("Enter Main Menu!");
        }

        public void Exit()
        {
            Debug.Log("Exit Main Menu!");
        }
    }
}