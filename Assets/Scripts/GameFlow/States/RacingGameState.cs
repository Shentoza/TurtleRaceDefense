﻿using Mesh;
using Services;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameFlow.States
{
    public class RacingGameState: MonoBehaviour, IGameFlowState
    {
        [SerializeField] private string _scenePath;
        
        public void Setup(IEventService eventService)
        {
            
        }

        public void Enter()
        {
            Debug.Log("Enter Racing State!");
            
            SceneManager.LoadScene(_scenePath);
        }

        public void Exit()
        {
            Debug.Log("Exit Racing State!");
        }
    }
}