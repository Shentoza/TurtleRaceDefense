﻿using System;
using Services;

namespace GameFlow
{
    [Serializable]
    public enum EGameFlowState
    {
        MainMenu,
        Loading,
        Build,
        Racing,
    }

    public class GameFlowState : IGameFlowState
    {
        public EGameFlowState State => _data.state;

        private readonly GameFlowStateData _data;

        public GameFlowState(GameFlowStateData data)
        {
            _data = data;
        }


        public void Setup(IEventService eventService)
        {
            // throw new System.NotImplementedException();
        }

        public void Enter()
        {
            
        }

        public void Exit()
        {
            
        }
    }
}