﻿using Mesh;
using Services;
using UnityEngine;

namespace GameFlow.States
{
    public class LoadingGameState: MonoBehaviour, IGameFlowState
    {
        public void Setup(IEventService eventService)
        {
            
        }

        public void Enter()
        {
            Debug.Log("Enter Loading State!");
        }

        public void Exit()
        {
            Debug.Log("Exit Loading State!");
        }
    }
}