﻿using Mesh;
using Services;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameFlow.States
{
    public class BuildGameState: MonoBehaviour, IGameFlowState
    {
        [SerializeField] private string _scenePath;
        
        public void Setup(IEventService eventService)
        {
            
        }

        public void Enter()
        {
            Debug.Log("Enter Build State!");

            SceneManager.LoadScene(_scenePath);
        }

        public void Exit()
        {
            Debug.Log("Exit Build State!");
        }
    }
}