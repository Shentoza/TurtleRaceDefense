﻿using GameFlow;
using UnityEngine;

[CreateAssetMenu(fileName = "StateData", menuName = "ScriptableObjects/GameFlowStateData", order = 0)]
public class GameFlowStateData : ScriptableObject
{
    public EGameFlowState state;
    public string scenePath;
    public GameObject uiPrefab;
}