﻿using Services;

namespace GameFlow
{
    public interface IGameFlowState
    {
        public void Setup(IEventService eventService);
        public void Enter();
        public void Exit();
    }
}