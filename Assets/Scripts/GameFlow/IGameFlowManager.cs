using System.Collections;
using System.Collections.Generic;
using GameFlow;
using UnityEngine;

public interface IGameFlowManager
{
    public void Setup();
    public void ChangeToState(EGameFlowState state);
    public void Shutdown();
}
