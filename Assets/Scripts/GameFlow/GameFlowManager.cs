﻿using System;
using System.Collections.Generic;
using GameFlow.States;
using Services;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameFlow
{
    public class GameFlowManager : MonoBehaviour, IGameFlowManager
    {
        [SerializeField] private MainMenuGameState _mainMenuGameStateData;
        [SerializeField] private LoadingGameState _loadingStateData;
        [SerializeField] private BuildGameState _buildStateData;
        [SerializeField] private RacingGameState _racingStateData;
        
        private Dictionary<EGameFlowState, IGameFlowState> _stateMappings = new Dictionary<EGameFlowState, IGameFlowState>();
        private EGameFlowState _currentState = EGameFlowState.MainMenu;
        private ServiceProvider _serviceProvider;
        
        public void Setup()
        {
            _serviceProvider = new ServiceProvider(new EventService());
            
            _mainMenuGameStateData.Setup(_serviceProvider.EventService);
            _loadingStateData.Setup(_serviceProvider.EventService);
            _buildStateData.Setup(_serviceProvider.EventService);
            _racingStateData.Setup(_serviceProvider.EventService);
            
            _stateMappings.Add(EGameFlowState.MainMenu, _mainMenuGameStateData);
            _stateMappings.Add(EGameFlowState.Loading, _loadingStateData);
            _stateMappings.Add(EGameFlowState.Build, _buildStateData);
            _stateMappings.Add(EGameFlowState.Racing, _racingStateData);
            
            ChangeToState(EGameFlowState.MainMenu);
        }

        public void ChangeToState(EGameFlowState state)
        {
            _stateMappings[_currentState].Exit();
            _currentState = state;
            _stateMappings[_currentState].Enter();
        }

        public void Shutdown()
        {
            
        }

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            Setup();
        }

        private void Start()
        {
            _serviceProvider.EventService.FireEvent(new GameStartedEvent("The game has started!"));
        }

        public void BtnClickedChangeToBuildState()
        {
            ChangeToState(EGameFlowState.Build);
        }
    }
}