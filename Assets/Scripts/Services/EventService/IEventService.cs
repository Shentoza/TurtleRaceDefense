﻿using System;

namespace Services
{
    public interface IEventService
    {
        public void FireEvent<T>(T eventType) where T : IEvent;
        public void SubscribeEvent<T>(Action<T> eventDelegate) where T : IEvent;
        public void UnsubscribeEvent<T>(Action<T> eventDelegate) where T : IEvent;
    }
}