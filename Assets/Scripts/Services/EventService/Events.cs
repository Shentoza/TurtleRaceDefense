﻿namespace Services
{
    public interface IEvent
    {
        
    }

    public class GameStartedEvent: IEvent
    {
        public string Message;
        
        public GameStartedEvent(string message)
        {
            Message = message;
        }
    }
}