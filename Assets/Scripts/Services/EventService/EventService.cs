﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Services
{
    public class EventService : IEventService
    {
        private readonly Dictionary<Type, List<Delegate>> _eventListeners = new Dictionary<Type, List<Delegate>>();
        
        // public delegate void Action<in T>(T obj);
            
        public void FireEvent<T>(T myEvent) where T : IEvent
        {
            Type type = typeof(T);

            if (_eventListeners.TryGetValue(type, out List<Delegate> existingListeners))
            {
                foreach (Delegate del in existingListeners)
                {
                    Action<T> actionHandler = del as Action<T>;
                    actionHandler?.Invoke(myEvent);
                }
            }
        }

        public void SubscribeEvent<T>(Action<T> eventDelegate) where T : IEvent
        {
            Type type = typeof(T);
            
            if (_eventListeners.TryGetValue(type, out List<Delegate> existingListeners))
            {
                if (existingListeners.Contains(eventDelegate))
                {
                    Debug.LogWarning($"Trying to add an event listener ({eventDelegate}) which was already added for type {type}!");
                }
                else
                {
                    existingListeners.Add(eventDelegate);
                }
            }
            else
            {
                _eventListeners[type] = new List<Delegate>() {eventDelegate};
            }
        }

        public void UnsubscribeEvent<T>(Action<T> eventDelegate) where T : IEvent
        {
            Type type = typeof(T);
            
            if (_eventListeners.TryGetValue(type, out List<Delegate> existingListeners))
            {
                if (existingListeners.Contains(eventDelegate))
                {
                    existingListeners.Remove(eventDelegate); // Empty list won't be removed this way.
                }
                else
                {
                    Debug.LogWarning($"Trying to remove an event listener ({eventDelegate}) which was not been added for type {type}!");
                }
            }
            else
            {
                Debug.LogWarning($"Trying to remove an event listener ({eventDelegate}) which was not been added for type {type}!");
            }
            
        }
    }
}