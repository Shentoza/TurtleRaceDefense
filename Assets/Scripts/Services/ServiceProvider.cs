﻿namespace Services
{
    public class ServiceProvider
    {
        public IEventService EventService => _eventService;
        
        private readonly IEventService _eventService;

        public ServiceProvider(IEventService eventService)
        {
            _eventService = eventService;
        }
    }
}